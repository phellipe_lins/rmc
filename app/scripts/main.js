gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(ScrollToPlugin);

(function($) {
  $(window).on('load', () => {
    /** 
     ================================== Thirdy Libs ==================================
    **/

    /* **** SLICK.JS **** */

    const slickParams = {
      dots: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      centerMode: true,
      autoplay: true,
      infinite: true,
      prevArrow: '<button class="slick-prev"><i class="icon icon--caret-left">caret_left</i></button>',
      nextArrow: '<button class="slick-next"><i class="icon icon--caret-right">caret_right</i></button>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    };

    $('#gallery__slider').slick(slickParams);

    $('#gallery__slider').on('init', () => {
      setTimeout(() => {
        $('#gallery__slider').slick('setPosition');
        console.log('setPosition')
      }, 5000);
    });

    $(window).on('resize', () => {
      if ($(window).width() > 1024) {
        $('#gallery__slider').slick('setPosition');
      }
    });

    /* **** INPUTMASK **** */

    $('input[type=tel]').inputmask('(99) 99999-9999');


    /** 
     ================================== Events ==================================
    **/

    /* **** STICK HEADER **** */

    $(window).on('load scroll mousewheel', () => {
      if ($(window).scrollTop() >= 120) {
        $('#header').addClass('header--sticky');
      } else {
        $('#header').removeClass('header--sticky');
      }
    })


    /* **** MENU OPEN **** */

    const menuBtnOpen = $('.open-menu');
    const menuBtnClose = $('.close-menu');
    const headerEl = $('#header')
    const menu = $('.menu')
    const menuItems = $('.menu__list li');

    menuBtnOpen.on('click', (event) => {
      console.log(event)
      event.preventDefault();

      menu.addClass('open');
      headerEl.addClass('open');
      gsap.fromTo(menuItems,
        { x: -100, opacity: 0 },
        { x: 0, opacity: 1, delay: 0.5, duration: 0.5, stagger: 0.1, ease: 'expo.out' },
        '-=0.4'
      )
    });

    menuBtnClose.on('click', (event) => {
      event.preventDefault();

      menu.removeClass('open');
      headerEl.removeClass('open');
      gsap.to(menuItems, { x: -100, opacity: 0 })
    });


    /* **** PRODUCTS EXPAND **** */

    $('.welcome__roll-down').on('click', (event) => {
      event.preventDefault();
      const targetID = $(event.target).attr('href');

      gsap.to(window, { duration: 1, scrollTo: { y: targetID, offsetY: 120 } });
    });

  
    /* **** PRODUCTS EXPAND **** */

    const productBox = $('.highlight__product');
    const productText = $('.highlight__product__text')

    $(productBox).on('mouseover', (event) => {
      event.preventDefault();
      event.stopPropagation();

      productBox.removeClass('active');
      $(event.target).addClass('active');
    });


    /* **** SIDEBAR **** */

    const indexAnchor = $('.indexes__anchor');
    const indexBorder = $('.active-border');
    const bannerAnchor = $('#banner');
    const bannerTarget = $('a[href=\'#banner\']');
    const galleryAnchor = $('#gallery');
    const galleryTarget = $('a[href=\'#gallery\']');
    const datasheetAnchor = $('#datasheet');
    const datasheetTarget = $('a[href=\'#datasheet\']');
    const progressAnchor = $('#progress');
    const progressTarget = $('a[href=\'#progress\']');
    const mapAnchor = $('#map');
    const mapTarget = $('a[href=\'#map\']');
    const offsetHeader = $('#header').innerHeight();

    $(indexAnchor).on('click', (event) => {
      event.preventDefault();
      event.stopPropagation();
      
      const indexAnchorID = $(event.target).attr('href');

      gsap.to(window, { duration: 1, scrollTo: { y: indexAnchorID, offsetY: 120 } });
    });

    $(window).on('scroll load', (event) => {
      const offsetTop = $(window).scrollTop();

      if ($('#banner').length) {
        if (offsetTop <= (bannerAnchor.innerHeight() - offsetHeader) + bannerAnchor.offset().top && offsetTop > (bannerAnchor.offset().top - offsetHeader)) {
          indexAnchor.removeClass('active');
          $(bannerTarget).addClass('active');
          gsap.to(indexBorder, { top: '0%' } );
        }

        if (offsetTop <= (galleryAnchor.innerHeight() - offsetHeader) + galleryAnchor.offset().top && offsetTop > (galleryAnchor.offset().top - offsetHeader)) {
          indexAnchor.removeClass('active');
          $(galleryTarget).addClass('active');
          gsap.to(indexBorder, { top: '20%' } );
        }

        if (offsetTop <= (datasheetAnchor.innerHeight() - offsetHeader) + datasheetAnchor.offset().top && offsetTop > (datasheetAnchor.offset().top - offsetHeader)) {
          indexAnchor.removeClass('active');
          $(datasheetTarget).addClass('active');
          gsap.to(indexBorder, { top: '40%' } );
        }

        if (offsetTop <= (progressAnchor.innerHeight() - offsetHeader) + progressAnchor.offset().top && offsetTop > (progressAnchor.offset().top - offsetHeader)) {
          indexAnchor.removeClass('active');
          $(progressTarget).addClass('active');
          gsap.to(indexBorder, { top: '60%' } );
        }

        if (offsetTop <= (mapAnchor.innerHeight() - offsetHeader) + mapAnchor.offset().top && offsetTop > (mapAnchor.offset().top - offsetHeader)) {
          indexAnchor.removeClass('active');
          $(mapTarget).addClass('active');
          gsap.to(indexBorder, { top: '80%' } );
        }
      }

    });


    /* **** GALLERY FILTER **** */

    $('.gallery__filter__selected').on('click', () => {
      $('.gallery__filter').toggleClass('open');
    })

    $('.gallery__filter__option span').on('click', (e) => {
      const text = $(e.target).text();
      const value = $(e.target).data('value');
      const filterBy = `.${value}`;

      $('.gallery__filter__selected span').text(text);
      $('.gallery__filter').removeClass('open');

      $('#gallery__slider').slick('slickUnfilter');
      if (value !== 'all') {
        $('#gallery__slider').slick('slickFilter', filterBy);
      }
    })


    /* **** CONTACT FORM PARTNER **** */

    const contactForm = $('#contact-form__form');
    const contactPartner = $('#contact-form__partner');
    const isPartner = window.location.hash;

    $('.contact-form__newsletter__button').on('click', (e) => {
      e.preventDefault();

      const btnText = $(e.target).text();

      if (btnText.toLowerCase() === 'seja nosso parceiro') {
        $(e.target).text('Entre em contato')
      } else {
        $(e.target).text('Seja nosso Parceiro')
      }

      gsap.to(window, { duration: 1, scrollTo: { y: '#contact-form__form', offsetY: 160 } });
      contactForm.toggleClass('active');
      contactPartner.toggleClass('active');
    });

    if (isPartner === '#partner') {
      $('.contact-form__newsletter__button').trigger('click');
    }


    /** 
     ================================== Animations ==================================
    **/

    /* **** HOME **** */

    const welcomeTitle = $('.welcome__text__title');
    const welcomeText = $('.welcome__text__description');
    const welcomeButton = $('.welcome__text__link');
    const welcomeImage = $('.welcome__image img');
    const welcomeBrand = $('.welcome__logo');
    const welcomeRollDownButton = $('.welcome__roll-down');

    if (welcomeTitle.length) {
      gsap.timeline({ delay: 1 })
        .fromTo(welcomeTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 })
        .fromTo(welcomeText, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(welcomeButton, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(welcomeImage, { opacity: 0, x: 100 }, { opacity: 1, x: 0, duration: 0.5, delay: 0.3 }, '-=0.3')
        .fromTo(welcomeBrand, { opacity: 0, x: -100 }, { opacity: 1, x: 0, duration: 0.5, delay: 0.3 }, '-=0.8')
        .fromTo(welcomeRollDownButton, { opacity: 0 }, { opacity: 1, duration: 0.5, delay: 0.5 })
    }

    const aboutHomeSubtitle = $('.about-home__subtitle');
    const aboutHomeTitle = $('.about-home__title');
    const aboutHomeText = $('.about-home__description');
    const aboutHomeButton = $('.about-home__link');
    const aboutHomeImage = $('.about-home__logo');

    if (aboutHomeTitle.length) {
      gsap.timeline({ scrollTrigger: { trigger: '.about-home__text', start: 'center bottom' } })
        .fromTo(aboutHomeSubtitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 })
        .fromTo(aboutHomeTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutHomeText, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutHomeButton, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutHomeImage, { opacity: 0, y: 100 }, { opacity: 1, y: 0, duration: 0.5 })
    }


    /* **** ABOUT **** */

    const aboutTitle = $('.about__title');
    const aboutText = $('.about__text');
    const aboutCresci = $('.about__creci');
    const aboutHighlight = $('.about__text__highlight');
    const aboutImage = $('.about__brand');

    if (aboutTitle.length) {
      gsap.timeline({ delay: 1 })
        .fromTo(aboutTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 })
        .fromTo(aboutText, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutCresci, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutHighlight, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.5 }, '-=0.3')
        .fromTo(aboutImage, { opacity: 0 }, { opacity: 1, duration: 0.5 })
    }

    const feature = $('.feature');

    if (feature.length) {
      gsap.timeline({ scrollTrigger: { trigger: '.features', start: 'center bottom' } })
        .fromTo(feature, { opacity: 0, y: 80 }, { opacity: 1, y: 0, duration: 0.4, stagger: 0.2 });
    }

    const portfolioTitle = $('.portfolio__article__title');
    const portfolioText = $('.portfolio__article__text');
    const portfolio = $('.portfolio__list__item');

    if (portfolio.length) {
      gsap.timeline({ scrollTrigger: { trigger: '.portfolio', start: 'center bottom' } })
        .fromTo(portfolioTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4 }, '-=0.2')
        .fromTo(portfolioText, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4 }, '-=0.2')
        .fromTo(portfolio, { opacity: 0, y: 80 }, { opacity: 1, y: 0, duration: 0.4, stagger: 0.2 });
    }


    /* **** PRODUCTS **** */


    /* **** PRODUCT **** */

    const content = $('#content.product');
    const header = $('#header.product');

    if (content.length && $(window).width() > 1024) {
      const productAnimation = gsap.timeline({ delay: 1 })
      .fromTo(header, { paddingLeft: '5%' }, { paddingLeft: '24%', duration: 0.8 })
      .fromTo(content, { paddingLeft: '0%' }, { paddingLeft: '20%', duration: 0.8 }, '-=0.6')
      .fromTo(sidebar, { left: '-20%' }, { left: '0%', duration: 0.8 }, '-=0.8')

      productAnimation.eventCallback('onComplete', () => {
        $('#gallery__slider').slick('setPosition');
      });
    }

    $(window).on('resize', () => {
      if ($(window).width() < 1024) {
        header.css('padding-left', '8%');
        content.css('padding-left', '0%');
      } else {
        header.css('padding-left', '5%');
        content.css('padding-left', '20%');
      }
    });

    const indexes = $('.indexes__anchor');

    if (indexes.length) {
      gsap.timeline({ delay: 2 })
        .fromTo(indexes, { opacity: 0, x: 50 }, { opacity: 1, x: 0, duration: 0.4, stagger: 0.2 })
    }

    const projectTitle = $('.project__title');
    const projectText1 = $('.project__text p').first();
    const projectText2 = $('.project__text p').last();

    if (projectTitle.length) {
      gsap.timeline({ scrollTrigger: { trigger: '#project', start: 'center bottom' } })
        .fromTo(projectTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4 })
        .fromTo(projectText1, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4 }, '-=0.2')
        .fromTo(projectText2, { opacity: 0, x: 50 }, { opacity: 1, x: 0, duration: 0.4 }, '-=0.2')
    }

    const playButton = $('.gallery__video__button .icon');

    if (playButton.length) {
      gsap.timeline({ scrollTrigger: { trigger: '.gallery__video', start: '-50%' } })
        .fromTo(playButton, { opacity: 0, scale: 0 }, { opacity: 1, scale: 1, duration: 0.4 });
    }

    const datasheetTitle = $('.datasheet__title');
    const datasheetItems = $('.datasheet__list li');

    if (datasheetTitle.length) {
      gsap.timeline({ scrollTrigger: { trigger: '#datasheet', start: 'center bottom' } })
        .fromTo(datasheetTitle, { opacity: 0, y: -50 }, { opacity: 1, y: 0, duration: 0.4 })
        .fromTo(datasheetItems, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4, stagger: 0.2 }, '-=0.1');
    }

    const progressTitle = $('.progress__title');
    const progressItems = $('.progress__sector');

    if (progressTitle.length) {
      gsap.timeline({ scrollTrigger: { trigger: '#progress', start: 'center bottom' } })
        .fromTo(progressTitle, { opacity: 0, x: -50 }, { opacity: 1, x: 0, duration: 0.4 })
        .fromTo(progressItems, { opacity: 0, y: 50 }, { opacity: 1, y: 0, duration: 0.4, stagger: 0.2 }, '-=0.1');
    }


    /* **** CONTACT **** */

    const contactTitle = $('.contact-form__title');
    const contactBox = $('.contact-form__box');

    if (contactTitle.length) {
      gsap.timeline({ delay: 1 })
        .fromTo(contactBox, { height: 0 }, { height: 400, duration: 0.4 })
        .fromTo(contactTitle, { opacity: 0, x: -80 }, { opacity: 1, x: 0, duration: 0.4 });
    }

    const newsletterArrow = $('.contact-links__newsletter__title i');

    if (newsletterArrow.length) {
      gsap.timeline({ scrollTrigger: { trigger: newsletterArrow, start: 'center bottom' } })
        .fromTo(newsletterArrow, { x: 80, opacity: 0 }, { x: 0, opacity: 1, duration: 0.4 });
    }

    const locationArrow = $('.contact-location__address__title i');

    if (locationArrow.length) {
      gsap.timeline({ scrollTrigger: { trigger: locationArrow, start: 'center bottom' } })
        .fromTo(locationArrow, { x: -80, opacity: 0 }, { x: 0, opacity: 1, duration: 0.4 });
    }
  })
})(jQuery);